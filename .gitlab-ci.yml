# Continuous Integration configuration with gitlab.com
# See https://gitlab.com/help/ci/yaml/README.md


#########################################################
# Default CI pipeline configuration:
# - preset variables: 'WITH_NANO_EXTENDED = 1' set in gitlab project
# - jobs run in stages:
#   - gen: generation job
#   - build: build-linux64-ASan
#   - tests:
#     - test-check
#     - test-unit
#     - test-uactt
#
# CI pipeline manual run with 'ALL_TESTS = 1':
# - preset variables: 'WITH_NANO_EXTENDED = 1' set in gitlab project
# - jobs run in stages:
#   - build: build-linux64-pys2opc # 'WITH_ASAN = 0' and 'WITH_PYS2OPC = 1'
#   - tests: # TEST_SERVER_XML_ADDRESS_SPACE and TEST_SERVER_XML_CONFIG set to use dynamic config
#     - test-unit-all-tests
#     - test-uactt-all-tests
#
# CI pipeline manual run with 'ALL_BUILDS = 1':
# - preset variables: WITH_NANO_EXTENDED = 1 set in gitlab project
# - jobs run in stages:
#   - gen: generation job
#   - build: # 'WITH_STATIC_SECURITY_DATA: 1', 'WITH_CONST_ADDSPACE: 1' and 'PUBSUB_STATIC_CONFIG: 1'
#     - build-linux64-static-conf
#   - tests:
#     - test-unit
#   - build-others:
#     - build-win32
#     - build-win64
#     - windows-appveyor
#
# CI pipeline manual run with 'WINDOWS_TEST = 1':
# - jobs run in stages:
#   - build-others:
#     - windows-appveyor
#
# CI pipeline manual run with 'CROSS_COMPILE = 1':
# - jobs run in stages:
#   - build-others:
#     - build-win32
#     - build-win64
#########################################################

# Variables for ALL_BUILDS and ALL_TESTS manual pipelines

.variables: &all-tests_build_variables
  WITH_PYS2OPC: 1

.variables: &all-tests_test_variables
  TEST_SERVER_XML_ADDRESS_SPACE: s2opc.xml
  TEST_SERVER_XML_CONFIG: S2OPC_Server_UACTT_Config.xml
  TEST_USERS_XML_CONFIG: S2OPC_UACTT_Users.xml

.variables: &all-builds_build_variables
  WITH_STATIC_SECURITY_DATA: 1
  WITH_CONST_ADDSPACE: 1
  PUBSUB_STATIC_CONFIG: 1

stages:
  - gen
  - build
  - test
  - build-others
  - analyses

.docker-job: &docker_job
  tags:
    - docker
    - linux

###
# Generation jobs
###

generation:
  <<: *docker_job
  stage: gen
  image: com.systerel.fr:5000/c765/gen-ext@sha256:29c8f93dfce606b86133605ee367c14e3b4b978eebe4181e42e9b826db06b06c # digest of gen-ext:1.3
  script:
    - ./clean.sh all
    - ./.pre-build-ext.sh
    - ./.check_generated_code.sh
  artifacts:
    name: 'bgenc-${CI_COMMIT_REF_SLUG}_${CI_COMMIT_SHA}'
    paths:
      - 'pre-build.log'
    when: always
    expire_in: '1 month'
    when: always
  rules:
    - if: "$NO_GEN == null && \
           $WINDOWS_TEST == null && \
           $CROSS_COMPILE == null && \
           $S2OPC_PUBSUB_ONLY == null &&\
           $ALL_BUILDS == null && \
           $ALL_TESTS == null"
      when: on_success

###
# Compilation jobs
###

.build-linux: &build_linux
  <<: *docker_job
  stage: build
  image: registry.gitlab.com/systerel/s2opc/build@sha256:66b648b78cc69397f95f025dfb34a4d7d1548c8364063aade1f44300b4f03a3f # digest of build:1.17
  script:
    - &build_command_line './build.sh && cd $BUILD_DIR && cmake --build . --target install || exit 1'
  variables: &build_linux_variables
    CMAKE_INSTALL_PREFIX: '../install_linux64'
    DOCKER_IMAGE: 'sha256:8898243017e0e43fff262fc11a8c1ae6b9284764fb4d6ee5a117696daeff5588'
    BUILD_DIR: build
  artifacts:
    name: 'bin-${CI_COMMIT_REF_SLUG}_${CI_COMMIT_SHA}'
    paths:
      - '*.log'
      - 'install_linux64/'
      - 'build/'  # Required when using coverage analysis
    expire_in: '1 month'
    when: always
# Common rules overwritten by each job using this template
#  rules: # note: should be repeated in each child job of *build_linux
#    - if: "$WINDOWS_TEST == null && $CROSS_COMPILE == null && $WITH_COVERITY == null"
#      when: on_success

# Used by "normal" pipeline
build-linux64-ASan:
  <<: *build_linux
  variables:
    <<: *build_linux_variables
    WITH_ASAN: 1
  rules: # 1st line repeat .build-linux + added rules to avoid ASAN build
    - if: "$WINDOWS_TEST == null && $CROSS_COMPILE == null && \
           $WITH_COVERITY == null && $WITH_COVERAGE == null && \
           $NO_ASAN == null && $ALL_TESTS == null && $ALL_BUILDS == null" # avoid ASAN build in most of nightly builds
      when: on_success

# Used by all-tests pipeline (add tests which needs NO_ASAN)
build-linux64-pys2opc:
  <<: *build_linux
  variables:
    <<: *build_linux_variables
    <<: *all-tests_build_variables # add pys2opc
  rules: # do not repeat .build-linux: due to disjunction + need only those 2 conditions
    - if: "$ALL_TESTS != null || $NO_ASAN != null && $WITH_NANO_EXTENDED == '1'" # if ASAN is deactivated build PyS2OPC by default (exclude nano scope: see TODO below)
      when: on_success

# Used when ALL_BUILDS set (tests use static configuration)
build-linux64-static-conf:
  <<: *build_linux
  variables:
    <<: *build_linux_variables
    <<: *all-builds_build_variables # add static conf
  rules: # single case rule of ALL_BUILDS
    - if: "$ALL_BUILDS != null"
      when: on_success

build-linux64-NoASan-NoPyS2OPC:
  <<: *build_linux
  variables:
    <<: *build_linux_variables
  rules: # In case of coverage analysis or when nano scope is used (TODO: PyS2OPC shall be built but tests will fail: use another server with subscription in tests)
    - if: "$WITH_COVERAGE != null || $NO_ASAN != null && $WITH_NANO_EXTENDED != '1'"
      when: on_success

build-win64: &build_win64
  <<: *docker_job
  stage: build-others
  image: registry.gitlab.com/systerel/s2opc/mingw_build@sha256:5f172f83a860caa75366e2ab14676be6f1d825441b0c61d363a6f872d1fb95c4 # digest of mingw_build:1.9
  script:
    # Manually disable PyS2OPC for windows cross builds. When set in "variables:", the value is overwritten.
    - 'export WITH_PYS2OPC=OFF'
    - *build_command_line
  variables: &build_win64_variables
    CMAKE_INSTALL_PREFIX: '../install_win64'
    CMAKE_TOOLCHAIN_FILE: 'toolchain-mingw32-w64_x86_64.cmake'
    DOCKER_IMAGE: 'sha256:dea49d2f60f0b9c2cff484d01f7372935d0e25dee3445909479b99d61530d1a8'
    BUILD_SHARED_LIBS: 'true'
    BUILD_DIR: build_toolchain
    S2OPC_CLIENTSERVER_ONLY: 1 # PubSub is not compatible with windows platform
  artifacts:
    name: 'bin-w64-${CI_COMMIT_REF_SLUG}_${CI_COMMIT_SHA}'
    paths:
      - '$BUILD_DIR/bin'
      - 'install_win64/'
    expire_in: '1 month'
  rules:
   - if: "$CROSS_COMPILE != null || $ALL_BUILDS != null || $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == 'master'"
     when: always # run even if previous job in pipeline failed

build-win32:
  <<: *build_win64
  variables:
    <<: *build_win64_variables
    CMAKE_INSTALL_PREFIX: '../install_win32'
    CMAKE_TOOLCHAIN_FILE: 'toolchain-mingw32-w64.cmake'
  artifacts:
    name: 'bin-w32-${CI_COMMIT_REF_SLUG}_${CI_COMMIT_SHA}'
    paths:
      - '$BUILD_DIR/bin/'
      - 'install_win32/'
    expire_in: '1 month'
  rules:
   - if: "$CROSS_COMPILE != null || $ALL_BUILDS != null || $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == 'master'"
     when: always # run even if previous job in pipeline failed

# Used by coverity, also starts the analysis
build-linux64-coverity:
  <<: *docker_job
  stage: build
  image: registry.gitlab.com/systerel/s2opc/coverity@sha256:e02d44536617d86f1a977ca2e35dd794e311fd9b3bedcca31d0681581399f058 # digest of coverity:1.4
  script: |-
    mkdir build
    cd build
    cmake -DWITH_COVERITY=1 -DCMAKE_BUILD_TYPE=Debug -DWITH_NANO_EXTENDED=1 ..
    /opt/cov-analysis-linux64-2019.03/bin/cov-build --dir cov-int make
    tar czf cov-int.tar.gz cov-int
    curl -k --form token=${COVERITY_TOKEN} --form email=${COVERITY_EMAIL} --form version=${CI_COMMIT_REF_SLUG}_${CI_COMMIT_SHA} --form description=${CI_COMMIT_REF_SLUG}_${CI_COMMIT_SHA} --form file=@cov-int.tar.gz https://scan.coverity.com/builds?project=S2OPC
  rules:
    - if: "$WITH_COVERITY != null"

###
# Test jobs
###

.test-linux: &test_linux
  <<: *docker_job
  stage: test
# Common rules overwritten by each job using this template
#  rules: # note to be repeated by each child job of *test_linux
#    - if: "$NO_TESTS == null && $WINDOWS_TEST == null && $CROSS_COMPILE == null && \
#           $WITH_COVERITY == null && $WITH_COVERAGE == null"
#      when: on_success

# Test check belongs to the test part, however it does not depend on linux build.
test-check:
  <<: *test_linux
  image: registry.gitlab.com/systerel/s2opc/check@sha256:ccbf1f60a6633bd64b3d1b89fa049ceee3c9272465a625587d815e08b0fc543d # digest of check:1.9
  script:
    - ./.check-code.sh
  artifacts:
    name: 'test-check-results-${CI_COMMIT_REF_SLUG}_${CI_COMMIT_SHA}'
    paths:
      - 'pre-build-check.log'
      - 'clang_tidy.log'
      - 'build-analyzer/analyzer-report'
    when: on_failure
    expire_in: '1 month'
  rules: # repeat .test-linux variables exception since overwritten here
    - if: "$NO_TESTS == null && $WINDOWS_TEST == null && $CROSS_COMPILE == null && \
           $WITH_COVERITY == null && $WITH_COVERAGE == null && \
           $ALL_TESTS == null && $ALL_BUILDS == null"
      when: on_success

.test-unit: &test_unit
  <<: *test_linux
  image: registry.gitlab.com/systerel/s2opc/test@sha256:b8fc17972b668928fa1053abe7aa970fa6d887580f02fc32bb9aa9f8b5366eb3  # digest of test:2.7
  script:
    - ./test-all.sh
  artifacts:
    name: 'test-unit-results-${CI_COMMIT_REF_SLUG}_${CI_COMMIT_SHA}'
    paths:
      - 'build/'
    when: always
    expire_in: '1 month'
# Common rules defined by test-unit job below

# Test unit for ALL_TESTS variable (add dynamic configuration)
test-unit-all-tests:
  <<: *test_unit
  variables:
    <<: *all-tests_test_variables
  script:
    - ./test-all.sh
  rules:
    - if: "$ALL_TESTS != null"
      when: on_success

# Test unit for NOT ALL_TESTS variable
test-unit:
  <<: *test_unit
  rules: # repeat .test-linux variables exception since overwritten here
    - if: "$NO_TESTS == null && $WINDOWS_TEST == null && $CROSS_COMPILE == null && \
           $WITH_COVERITY == null && \
           $ALL_TESTS == null" # + excludes ALL_TESTS
      when: on_success

.test-uactt: &test_uactt
  <<: *test_linux
  image: com.systerel.fr:5000/c838/uactt-win@sha256:e8a51f56722873c5752f8e50f33e557db426a14460558bc5931b9d24e7a9b540 # digest of uactt-win:1.7
  script:
    - 'cd tests/ClientServer/acceptance_tools/ && ./launch_acceptance_tests.sh'
  artifacts:
    name: 'uactt-logs-${CI_COMMIT_REF_SLUG}_${CI_COMMIT_SHA}'
    paths:
      - 'build/'
      - 'tests/ClientServer/acceptance_tools/*.log'
    when: always
    expire_in: '1 month'

# Test unit for ALL_TESTS variable (add dynamic configuration)
test-uactt-all-tests:
  <<: *test_uactt
  variables:
    <<: *all-tests_test_variables
  rules:
    - if: "$ALL_TESTS != null && \
           $S2OPC_PUBSUB_ONLY == null && \
           $ALL_BUILDS == null" # + no UACTT tests on PubSub + excludes ALL_BUILDS

# Test unit for NOT ALL_TESTS variable
test-uactt:
  <<: *test_uactt
  rules: # repeat .test-uactt variables exception since overwritten here
    - if: "$NO_TESTS == null && $WINDOWS_TEST == null && $CROSS_COMPILE == null && \
           $WITH_COVERITY == null && $WITH_COVERAGE == null && \
           $S2OPC_PUBSUB_ONLY == null && $ALL_BUILDS == null && $ALL_TESTS == null"
           # + no UACTT tests on PubSub + excludes ALL_BUILDS + excludes ALL_TESTS
      when: on_success

##
# Windows native build and tests (done in "build-others" stage)
##

windows-appveyor:
  stage: build-others
  image: docker.io/library/debian@sha256:de3eac83cd481c04c5d6c7344cd7327625a1d8b2540e82a8231b5675cef0ae5f  # debian:9
  script:
    - 'apt-get update && apt-get -y install curl && curl -H "Authorization: Bearer $APPVEYOR_TOKEN" -H "Content-Type: application/json" --request POST -d ''{"accountName" : "Systerel", "projectSlug": "s2opc", "branch": "''$CI_COMMIT_REF_NAME''"}'' https://ci.appveyor.com/api/builds'
  tags:
    - linux
  rules:
    - if: "$ALL_BUILDS != null || $WINDOWS_TEST != null || \
           $CI_COMMIT_BRANCH == 'master' && \
           $ALL_TESTS == null && $WITH_COVERITY == null && $WITH_COVERAGE == null && \
           $NO_ASAN == null && $NO_GEN == null && \
           $S2OPC_PUBSUB_ONLY == null && $S2OPC_CLIENTSERVER_ONLY == null" # Avoid to run job on master for all nightly build except ALL_BUILDS
      when: always # run even if previous job in pipeline failed

###
# Analysis jobs
###

coverage-analysis:
  <<: *docker_job
  stage: analyses
  image: registry.gitlab.com/systerel/s2opc/test@sha256:b8fc17972b668928fa1053abe7aa970fa6d887580f02fc32bb9aa9f8b5366eb3 # digest of test:2.7
  script:
    - ./.gen_coverage.sh
  artifacts:
    name: 'coverage-report-${CI_COMMIT_REF_SLUG}_${CI_COMMIT_SHA}'
    paths:
      - 'report'
    expire_in: '1 month'
  rules:
    - if: "$WITH_COVERAGE != null"
